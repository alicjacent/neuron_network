package pl.alicja.siecneuronowa;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Siec {

	private static final int ILOSC_NEURONOW_UKRYTYCH = 8;
	private static final int ILOSC_EPOK = 500;
	private static final int RAND_MAX = 100;
	private double blad = 0;

	// warstwy
	private NeuronWejsciowy wejsciowy = new NeuronWejsciowy();
	private List<NeuronUkryta> ukryta = new ArrayList<>();
	private NeuronWyjsciowy wyjsciowy = new NeuronWyjsciowy(ILOSC_NEURONOW_UKRYTYCH);

	// tablica z prawidlowymi wartosciami pierwiastkow
	private double[][] tablicaTreningowa = new double[2][50];
        
	// tablica, do ktorej zostana wstawione wartosci obliczone przez nauczona
	// wczesniej siec neuronowa
	private double[][] tablicaObliczeniowa = new double[2][100];

	// tablica z bledami sredniokwadratowymi
	private double[][] bledySredniokwadratowe = new double[2][ILOSC_EPOK];

	public Siec() {
		setWarstwaUkryta();
		uzupelnijTabliceTreningowa();
		uzupelnijTabliceObliczeniowa();
	}

	private void setWarstwaUkryta() {
		for (int i = 0; i < ILOSC_NEURONOW_UKRYTYCH; i++) {
			ukryta.add(new NeuronUkryta());
		}
	}

	// uzupelniamy tablice treningowa liczbami i wartosciami pierwiastkow
	private void uzupelnijTabliceTreningowa() {
		Random rand = new Random();
		double value;
		List<Double> values = new ArrayList<>();
		for (int k = 0; k < 50; k++) {
			do {
				value = rand.nextInt(RAND_MAX + 1);
			} while (values.contains(value));
			values.add(value);
			tablicaTreningowa[0][k] = value;
			tablicaTreningowa[1][k] = Math.sqrt(value);
		}
	}

	// podajemy do tablicy wartosci 1-100 w pierwszym wierszu, pozniej w drugim
	// wierszu bedziemy wpisywac w petli obliczone wartosci pierwiastkow (w
	// testowaniu)
	private void uzupelnijTabliceObliczeniowa() {
		for (int i = 0; i < 100; i++) {
			tablicaObliczeniowa[0][i] = i + 1;
		}
	}

	private void propagacjaWPrzod(double liczbaDoPierwiastkowania) {
		wejsciowy.obliczWyjscie(liczbaDoPierwiastkowania);
		for (int i = 0; i < ukryta.size(); i++) {
			ukryta.get(i).obliczWyjscie(wejsciowy.getWyjscie());
			wyjsciowy.getWejscia()[i] = ukryta.get(i).getWyjscie();
		}
		wyjsciowy.obliczWyjscie();
	}

	private void propagacjaWTyl(double oczekiwanaWartoscPierwiastka) {
		wyjsciowy.obliczBlad(oczekiwanaWartoscPierwiastka);
		for (int i = 0; i < ukryta.size(); i++) {
			ukryta.get(i).obliczBlad(wyjsciowy.getBlad() * wyjsciowy.getWagi()[i]);
			ukryta.get(i).liczNoweWagiIBiasy();
		}
		wyjsciowy.liczNoweWagiIBiasy();
	}

	public double[][] getBledySredniokwadratowe() {
		return bledySredniokwadratowe;
	}

	public void ucz() {
		for (int i = 0; i < ILOSC_EPOK; i++) {
			for (int j = 0; j < 50; j++) {
				propagacjaWPrzod(tablicaTreningowa[0][j]);
				propagacjaWTyl(tablicaTreningowa[1][j]);
				blad = blad + obliczBladSredniokwadratowy(tablicaTreningowa[1][j], wyjsciowy.getWyjscie());
			}
			blad = blad / 50;
			bledySredniokwadratowe[0][i] = i;
			bledySredniokwadratowe[1][i] = blad;
		}
	}

	public double[][] getTablicaTreningowa() {
		return tablicaTreningowa;
	}

	public double[][] getTablicaObliczeniowa() {
		return tablicaObliczeniowa;
	}

	public void testuj() {
		for (int i = 0; i < 100; i++) {
			propagacjaWPrzod(tablicaObliczeniowa[0][i]);
			tablicaObliczeniowa[1][i] = wyjsciowy.getWyjscie();
			System.out.println("Liczba: " + tablicaObliczeniowa[0][i] + "\tWartosc: " + tablicaObliczeniowa[1][i]);
		}
	}

	private double obliczBladSredniokwadratowy(double oczekiwana, double uzyskana) {
		double blad = 0;
		blad = (oczekiwana - uzyskana) * (oczekiwana - uzyskana);
		return blad;
	}

}
