package pl.alicja.siecneuronowa;

public abstract class Neuron {
    
    protected static final double ALFA = 0.1;
    protected static final double MOMENTUM = 0.1;

    protected double blad;
    protected double bias;
    protected double staryBias;
    protected double suma;
    protected double wyjscie;


    //obliczenie wartosci wyjscia
    public abstract void aktywacja(double suma); 
    

    public double getBlad() {
        return blad;
    }

    public void setBlad(double blad) {
        this.blad = blad;
    }

    public double getBias() {
        return bias;
    }

    public void setBias(double bias) {
        this.bias = bias;
    }

    public double getStaryBias() {
        return staryBias;
    }

    public void setStaryBias(double staryBias) {
        this.staryBias = staryBias;
    }

    public double getSuma() {
        return suma;
    }

    public double getWyjscie() {
        return wyjscie;
    }
    
}
