package pl.alicja.siecneuronowa;

public class NeuronWejsciowy extends Neuron {

    private double wejscie;
    private double waga;
    private double staraWaga;
    
    public NeuronWejsciowy() {
        super();
        bias = 0;
        initializeWaga();
    }

    @Override
    public void aktywacja(double suma) {
        wyjscie = suma * 1;
    }

    public void obliczWyjscie(double liczbaDoPierwiastkowania) {
        wejscie = liczbaDoPierwiastkowania;
        suma = waga * wejscie + bias;
        aktywacja(suma);
    }
   
    private double initializeWaga() {
        waga = 1;
        return waga;
    }

    public double getWejscie() {
        return wejscie;
    }

    public double getWaga() {
        return waga;
    }

    public void setWaga(double waga) {
        this.waga = waga;
    }

    public double getStaraWaga() {
        return staraWaga;
    }

    public void setStaraWaga(double staraWaga) {
        this.staraWaga = staraWaga;
    }
    
}
