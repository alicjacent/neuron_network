package pl.alicja.siecneuronowa;

import java.util.Random;

public class NeuronUkryta extends Neuron {

    //do losowania wagi i bias (do uzycia przy pierwszej iteracji nauki sieci)
    private static final double RAND_MIN = -0.5;
    private static final double RAND_MAX = 0.5;
    private static final double BETA = 0.01;
    
    private double wejscie;
    private double waga;
    private double staraWaga;

    public NeuronUkryta() {
        super();
        initializeBias();
        initializeWaga();
        staraWaga = waga;
        staryBias = bias;
    }

    @Override
    public void aktywacja(double suma) {
        wyjscie = 1 / (1 + Math.exp(-1 * BETA * suma));
    }

    protected double initializeWaga() {
        Random rand = new Random();
        waga = rand.nextDouble() * (RAND_MIN + (RAND_MAX - RAND_MIN));
        return waga;
    }

    private double initializeBias() {
        Random rand = new Random();
        bias = rand.nextDouble() * (RAND_MIN + (RAND_MAX - RAND_MIN));
        return bias;
    }

    public void obliczWyjscie(double wartoscNaWejscie) {
        wejscie = wartoscNaWejscie;
        suma = wejscie * waga + bias;
        aktywacja(suma);
    }
    
    //iloczyn - blad neuronu wyjsciowego * odpowiednia waga neuronu
    public void obliczBlad(double iloczyn) {
        blad = iloczyn * wyjscie * (1-wyjscie) * BETA;
    }
    
    public void liczNoweWagiIBiasy() {
        //zapamietanie starych wartosci
        double zapamietajWage = waga;
        double zapamietajBias = bias;
        //obliczenie nowych wartosci
        waga = waga + ALFA * blad * wejscie + (MOMENTUM * (waga - staraWaga));
        bias = bias + ALFA * blad + (MOMENTUM * (bias - staryBias));
        //przepisanie wartosci, ktore byly 'wejsciowe' do funkcji jako stare wartosci
        staraWaga = zapamietajWage;
        staryBias = zapamietajBias;
    }
    
    public double getWejscie() {
        return wejscie;
    }

    public double getWaga() {
        return waga;
    }

    public void setWaga(double waga) {
        this.waga = waga;
    }

    public double getStaraWaga() {
        return staraWaga;
    }

    public void setStaraWaga(double staraWaga) {
        this.staraWaga = staraWaga;
    }
    
    
    
    
}
