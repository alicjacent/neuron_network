package pl.alicja.siecneuronowa;

import java.util.Random;

public class NeuronWyjsciowy extends Neuron {

    //do losowania wagi i bias (do uzycia przy pierwszej iteracji nauki sieci)
    private static final double RAND_MIN = -0.5;
    private static final double RAND_MAX = 0.5;
    private double[] wejscia;
    private double[] wagi;
    private double[] stareWagi;
    
    public NeuronWyjsciowy(int wejsciaRozmiar) {
        super();
        initializeBias();
        wejscia = new double[wejsciaRozmiar];
        wagi = new double[wejsciaRozmiar];
        initializeWaga();
        stareWagi = wagi;
        staryBias = bias;
    }

    @Override
    public void aktywacja(double suma) {
        wyjscie = suma * 1;
    }

    protected double[] initializeWaga() {
        Random rand = new Random();
        for (int i = 0; i < wagi.length; i++) {
            wagi[i] = rand.nextDouble() * (RAND_MIN + (RAND_MAX - RAND_MIN));
        }
        return wagi;
    }

    public double initializeBias() {
        Random rand = new Random();
        bias = rand.nextDouble() * (RAND_MIN + (RAND_MAX - RAND_MIN));
        return bias;
    }
    
    private void sumator() {
        suma = 0;
        for (int i = 0; i < wejscia.length; i++) {
            suma = wagi[i] * wejscia[i];
        }
        suma = suma + bias;
    }

    public void obliczWyjscie() {
        sumator();
        aktywacja(suma);
    }
    
    public void obliczBlad(double wartoscOczekiwana) {
        blad = wartoscOczekiwana - wyjscie;
    }
    
    public void liczNoweWagiIBiasy() {
        double[] zapamietajWagi = wagi;
        double zapamietajBias = bias;
        for(int i=0; i<wagi.length; i++) {
            wagi[i] = wagi[i] + ALFA * blad * wejscia[i] + (MOMENTUM * (wagi[i] - stareWagi[i]));
        }
        bias = bias + ALFA * blad + (MOMENTUM * (bias - staryBias));
        stareWagi = zapamietajWagi;
        staryBias = zapamietajBias;
        
    }
    
    public double[] getWejscia() {
        return wejscia;
    }
    
    public double[] getWagi() {
        return wagi;
    }

    public void setWagi(double[] wagi) {
        this.wagi = wagi;
    }

    public double[] getStareWagi() {
        return stareWagi;
    }

    public void setStareWagi(double[] stareWagi) {
        this.stareWagi = stareWagi;
    }
    
}
